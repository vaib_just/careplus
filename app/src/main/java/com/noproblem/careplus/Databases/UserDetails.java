package com.noproblem.careplus.Databases;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class UserDetails {

    public String name = null;
    public String email = null;
    public long phoneNumber = 0;
    public String address = null;
    public String gender = null;
    public LatLng latLng = null;
    public String bloodGroup = null;
    public Date dob = null;

    //Constructor 1 to update details during signup
    public UserDetails(String name, String email, Date dob) {
        this.name = name;
        this.email = email;
        this.dob = dob;
    }

    public UserDetails() {}

    public UserDetails(String name, long phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    //Method to modify latitude and longitude
    public void setLatitudeLongitude(LatLng latLng) {
        this.latLng = latLng;
    }

    //Method to modify blood group
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    //Method to modify address
    public void setAddress(String address) {
        this.address = address;
    }


    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public void setDob(Date dob) {
        this.dob = dob;
    }
}
