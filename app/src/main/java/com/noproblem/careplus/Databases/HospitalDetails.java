package com.noproblem.careplus.Databases;

import java.util.ArrayList;

public class HospitalDetails {

    public String address;
    public ArrayList<Long> contact;
    public String hospitalName;
    public String registrationNumber;
    public ArrayList<String> numberOfAmbulance;

    public  HospitalDetails() {}

    public HospitalDetails(String address,ArrayList<Long> contact, String hospitalName,ArrayList<String> numberOfAmbulance){
        this.address = address;
        this.contact = contact;
        this.hospitalName = hospitalName;
        this.numberOfAmbulance = numberOfAmbulance;
    }
}
