package com.noproblem.careplus.Databases;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class OrgDesDetails
{
    public String duration;
    public String distance;
    public ArrayList<LatLng> points;
    public LatLng position;

    public OrgDesDetails(String duration, String distance, ArrayList<LatLng> points, LatLng position) {
        this.duration = duration;
        this.distance = distance;
        this.points = points;
        this.position = position;
    }

    public OrgDesDetails() {

    }

    public ArrayList<LatLng> getPoints() {
        return points;
    }

    public LatLng getPosition() {
        return position;
    }

    public String getDistance() {
        return distance;
    }

    public String getDuration() {
        return duration;
    }
}
