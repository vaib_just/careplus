package com.noproblem.careplus.Databases;

import com.google.android.gms.maps.model.LatLng;

public class AmbulanceDetails {
    public long contact = 0;
    public String id;
    public boolean duty = false;
    public String email;
    public String driverName = "None", hospitalName,hospitalRegNum = null, licensePlateNo = null, type = null, driverContact;
    public LatLng location = new LatLng(0.123,0.123);
    public String bookingId = "None";

    public AmbulanceDetails(String id, long contact, String hospitalRegNum, String licensePlateNo, String type, boolean duty, String driverName, LatLng location) {
        //The fields not accepted via constructor will be fetched live
        this.id = id;
        this.contact = contact;
        this.hospitalRegNum = hospitalRegNum;
        this.licensePlateNo = licensePlateNo;
        this.type = type;
        this.duty = duty;
        this.driverName = driverName;
        this.location = location;
    }

    public AmbulanceDetails() {
    }

    public AmbulanceDetails(LatLng location,String licensePlateNo, String driverName, long contact, boolean duty) {
    }

    public AmbulanceDetails(String hospitalRegNum, long contact, String email, String licensePlateNo, String type)
    {
        this.hospitalRegNum = hospitalRegNum;
        this.contact = contact;
        this.email = email;
        this.licensePlateNo = licensePlateNo;
        this.type = type;
    }

    public LatLng getLocation() {
        return location;
    }
}
