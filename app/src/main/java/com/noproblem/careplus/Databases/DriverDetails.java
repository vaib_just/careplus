package com.noproblem.careplus.Databases;

public class DriverDetails {
    public String ambulanceId;
    public String name;
    public long number;

    public DriverDetails() {}

    public DriverDetails(String ambulanceId, String name, long number) {
        this.ambulanceId = ambulanceId;
        this.name = name;
        this.number = number;
    }
}
