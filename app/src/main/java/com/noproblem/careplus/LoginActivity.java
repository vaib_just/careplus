package com.noproblem.careplus;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Toast;

import com.noproblem.careplus.Firebase.FirebaseAuthentication;
import com.noproblem.careplus.Location.MapUserInterface;
import com.noproblem.careplus.Tools.AlertDialogBox;
import com.noproblem.careplus.Tools.CustomViewPager;
import com.noproblem.careplus.Tools.ViewPagerAdapter;


public class LoginActivity extends BaseActivity {

    private static final int LOGIN_FRAGMENT = 0;
    private static final int SIGNUP_FRAGMENT = 1;
    private static final int RESET_PASSWORD_FRAGMENT = 2;
    private static final int AMBULANCE_SIGNUP_FRAGMENT = 3;
    private CustomViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    FirebaseAuthentication authentication;
    MapUserInterface mapUserInterface;
    AlertDialogBox dialogBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // MapUserInterface AppPermissions & Internet,GPS Settings
        mapUserInterface = new MapUserInterface();
        mapUserInterface.getLocationPermission(this);
        mapUserInterface.getCallingPermission(this);
        mapUserInterface.checkNetworkAndGPSConnectivity(this);

        dialogBox = new AlertDialogBox(this);
        viewPager = findViewById(R.id.viewpager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setPagingEnabled(false);
        authentication = new FirebaseAuthentication(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        authentication.attachListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(authentication != null)
            authentication.detachListener();
    }

    private void changeFragment(int fragmentType) {

        switch (fragmentType) {
            case LOGIN_FRAGMENT:
                viewPager.setCurrentItem(LOGIN_FRAGMENT);
                break;
            case SIGNUP_FRAGMENT:
                viewPager.setCurrentItem(SIGNUP_FRAGMENT);
                break;
            case RESET_PASSWORD_FRAGMENT:
                viewPager.setCurrentItem(RESET_PASSWORD_FRAGMENT);
                break;
            case AMBULANCE_SIGNUP_FRAGMENT:
                viewPager.setCurrentItem(AMBULANCE_SIGNUP_FRAGMENT);
                break;
            default:
                viewPager.setCurrentItem(LOGIN_FRAGMENT);
                break;
        }


    }

    public void signUpClick(View view) {
        changeFragment(SIGNUP_FRAGMENT);
    }

    public void signInClick(View view) {
        changeFragment(LOGIN_FRAGMENT);
    }

    public void resetPasswordClick(View view) {
        changeFragment(RESET_PASSWORD_FRAGMENT);
    }

    public void hospitalSignUpClick(View view){
        changeFragment(AMBULANCE_SIGNUP_FRAGMENT);
    }

    public void backClick(View view){
        changeFragment(LOGIN_FRAGMENT);
    }

    @Override
    public void onBackPressed() {

        if (viewPager.getCurrentItem() == LOGIN_FRAGMENT)
        {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
        else {
            changeFragment(LOGIN_FRAGMENT);
        }
    }

    public void logInButtonClicked() {
        Toast.makeText(this, R.string.login_button_click, Toast.LENGTH_SHORT).show();
        }

    public void signUpButtonClicked() {
        Toast.makeText(this, R.string.signup_button_click, Toast.LENGTH_SHORT).show();
        // Register in Database
    }

    public void resetPasswordButtonClicked() {
        Toast.makeText(this, R.string.reset_password_button_clicked, Toast.LENGTH_SHORT).show();
        // Re
    }
}
