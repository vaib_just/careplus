
package com.noproblem.careplus;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.noproblem.careplus.Databases.UserDetails;
import com.noproblem.careplus.Location.MapUserInterface;
import com.noproblem.careplus.Tools.DirectionsJSONParser;
import com.noproblem.careplus.Tools.StringValidation;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import static android.content.Intent.ACTION_CALL;
import static com.noproblem.careplus.Location.MapUserInterface.locationManager;


public class AmbulanceMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private MapUserInterface mapUserInterface;

    //Firebase Variables to write data into user's database
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference currentAmbulanceDetails;

    Button amuSignOut;
    Button bookedButton;
    Switch dutyON;

    Boolean bookingStatus = false, dutyStatus = false, cancelStatus = false;

    TextView custNameView, custPhoneView, dutyTextView;
    EditText currentDriverName, currentDriverNumber;
    Button getDirections, callClient;

    String bookedClientName;
    Long bookedClientNumber;

    LatLng bookedLocation;
    FusedLocationProviderClient fusedClient;
    MarkerOptions markerOptions;
    Marker currentAmbulanceMarker, userMarker;
    LocationListener locationListener;

    String previousBookingId;

    @BindView(R.id.ambulancebottom_sheet)
    LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.AmbulanceMap);

        amuSignOut = findViewById(R.id.signOut);
        bookedButton = findViewById(R.id.bookButton);
        custNameView = findViewById(R.id.customerName);
        custPhoneView = findViewById(R.id.customerPhone);
        getDirections = findViewById(R.id.getDirections);
        callClient = findViewById(R.id.helloCustomer);
        dutyON = findViewById(R.id.dutyONoff);
        dutyTextView = findViewById(R.id.dutyText);
        dutyTextView.setText("Duty OFF");

        ButterKnife.bind(this);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    dutyTextView.setText("Duty OFF");
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        getDutyON();
        dutyON.setChecked(false);

        //Define location listener
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                //Display ambulance current location marker
                final DatabaseReference ambulanceLocation = currentAmbulanceDetails.child("location");

                if(currentAmbulanceMarker != null)
                    currentAmbulanceMarker.remove();
                markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(location.getLatitude(),location.getLongitude()));
                markerOptions.title("You");
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_amb));
                currentAmbulanceMarker = MapUserInterface.mMap.addMarker(markerOptions);
                //Save location to firebase
                ambulanceLocation.setValue(new LatLng(location.getLatitude(),location.getLongitude()));
                ambulanceCanceledListener();
                getBookingStatus();
                if(bookingStatus && userMarker == null) {
                    float[] results = new float[1];
                    Location.distanceBetween(currentAmbulanceMarker.getPosition().latitude, currentAmbulanceMarker.getPosition().longitude,
                            bookedLocation.latitude, bookedLocation.longitude, results);
                    if(results[0] < 15) {
                        mapUserInterface.mMap.clear();
                        bookingStatus = false;
                        CameraUpdateFactory.newLatLngZoom(currentAmbulanceMarker.getPosition(),15);
                        Toast.makeText(AmbulanceMapsActivity.this, "You've reached", Toast.LENGTH_SHORT).show();
                        //Set bookingId to false in firebase
                        currentAmbulanceDetails.child("bookingId").setValue(null);
                    }
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        amuSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amuSignOut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(userMarker != null)
                            Toast.makeText(AmbulanceMapsActivity.this, "Cannot logout while ambulance is booked", Toast.LENGTH_SHORT).show();
                        else {
                            FirebaseAuth.getInstance().signOut();
                            Intent intent = new Intent(AmbulanceMapsActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                            Toast.makeText(AmbulanceMapsActivity.this, "Successfully logged out", Toast.LENGTH_SHORT).show();
                            startActivity(intent);
                            finish();
                        }
                    }
                });
            }
        });


        mapUserInterface = new MapUserInterface();

        firebaseDatabase = FirebaseDatabase.getInstance();
        currentAmbulanceDetails = firebaseDatabase.getReference().child("Ambulance").
                child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        fusedClient = LocationServices.getFusedLocationProviderClient(this);

        //Set bookingId to false in firebase
        currentAmbulanceDetails.child("bookingId").setValue(null);

        mapFragment.getMapAsync(this);
        MapUserInterface mapUserInterface = new MapUserInterface();
        mapUserInterface.getLocationPermission(this);
        mapUserInterface.checkNetworkAndGPSConnectivity(this);
    }

    @Override
    protected void onResume() {
        //Display ambulance markers on map
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(AmbulanceMapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(AmbulanceMapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION},0);
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        super.onResume();
    }

    @Override
    protected void onPause() {
        locationManager.removeUpdates(locationListener);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Set duty and driver name to none in fire-base
        currentAmbulanceDetails.child("duty").setValue(false);
        currentAmbulanceDetails.child("driverName").setValue("None");
        currentAmbulanceDetails.child("driverContact").setValue(0);
        currentAmbulanceDetails.child("bookingId").setValue(null);
        userMarker = null;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final String TAG = AmbulanceMapsActivity.class.getSimpleName();
        mapUserInterface.mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
            mapUserInterface.updateLocationUI();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mapUserInterface.mMap.setMyLocationEnabled(false);

            if (!success) {
                //Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            //Log.e(TAG, "Can't find style. Error: ", e);
        }
        setMarker();
        getBookingStatus();

        //Listener to get all the ambulance details from database
        // Error : Set marker on dragging not working
        /*mapUserInterface.mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                bookingMarker.setPosition(marker.getPosition());
                mapUserInterface.mMap.animateCamera(CameraUpdateFactory.newLatLng(bookingMarker.getPosition()));
            }
        });
        */
    }

    public void getBookedButton() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + bookedLocation.latitude + ", " + bookedLocation.longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        }, 1000);
    }

    public void getCallDriverID() {

        Intent callIntent = new Intent(ACTION_CALL);
        if (bookedClientNumber != 0) {
            callIntent.setData(Uri.parse("tel:" + bookedClientNumber));
            startActivity(callIntent);
            Toast.makeText(AmbulanceMapsActivity.this, "Calling Customer", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Sorry, no number found!", Toast.LENGTH_SHORT).show();
        }
        if (ActivityCompat.checkSelfPermission(AmbulanceMapsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(AmbulanceMapsActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 0);

        }
    }

    public void getDutyON() {

        dutyON.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dutyTextView.setText("Duty ON");
                    CustomDialogBox();
                    sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
                    BottomSheetBehavior.from(layoutBottomSheet).setHideable(false);
                    sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                        @Override
                        public void onStateChanged(@NonNull View bottomSheet, int newState) {
                            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                                sheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);
                            }
                        }

                        @Override
                        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        }
                    });


                } 
                else if(userMarker == null){
                    dutyTextView.setText("Duty OFF");
                    //Set duty state to false in firebase
                    dutyStatus = false;
                    getBookingStatus();
                    //Set ambulance's duty as off
                    currentAmbulanceDetails.child("duty").setValue(dutyStatus);
                    currentAmbulanceDetails.child("driverName").setValue("None");
                    currentAmbulanceDetails.child("driverContact").setValue(0);
                    sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                        @Override
                        public void onStateChanged(@NonNull View bottomSheet, int newState) {
                            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            }
                        }
                        @Override
                        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                        }
                    });
                }
                else {
                    dutyON.setChecked(true);
                    Toast.makeText(AmbulanceMapsActivity.this, "Cannot cancel booking in between", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //Custom Dialog box to input driver name and duty
    public void CustomDialogBox() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Enter driver details");
        final StringValidation validation = new StringValidation();

        //final EditText input = new EditText(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View view = layoutInflater.inflate(R.layout.custom_dialog_box, null);
        alertDialog.setView(view);

        final EditText cDriverName = view.findViewById(R.id.currentDriverName);

        final EditText cDriverNumber = view.findViewById(R.id.currentDriverNumber);

        alertDialog.setPositiveButton("Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dutyStatus = true;
                        dutyON.setChecked(true);
                    }

                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dutyON.setChecked(false);
                        }
                });

        final AlertDialog dialog = alertDialog.create();
        dialog.setCancelable(false);
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean wantToCloseDialog = false;
                String cDriver = cDriverName.getText().toString();
                String cDriverNum = cDriverNumber.getText().toString();

                if (cDriver != null &&
                        validation.isValidName(cDriver)) {
                    wantToCloseDialog = true;
                } else {
                    cDriverName.setError("Invalid Name");
                    wantToCloseDialog = false;
                }
                if (cDriverNum.length() != 10) {
                    cDriverNumber.setError("Phone should be 10 digit long");
                    wantToCloseDialog = false;
                } else {
                    wantToCloseDialog = true;
                }

                if (wantToCloseDialog)
                {
                    dutyStatus = true;
                    //Store driver details in firebase
                    currentAmbulanceDetails.child("driverName").setValue(cDriver);
                    //Store driver num in firebase
                    currentAmbulanceDetails.child("driverContact").setValue(cDriverNum);
                    //Set ambulance's duty as on
                    currentAmbulanceDetails.child("duty").setValue(dutyStatus);
                    //Get the location of client if the ambulance is booked
                    getBookingStatus();
                    dialog.dismiss();
                }
            }
        });
    }

    //Custom dialog to allow driver to accept booking
    public void CustomDialog(String userName) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        final MediaPlayer ring= MediaPlayer.create(this,R.raw.beeps_beeps_simple);
        ring.setLooping(true);
        ring.start();
        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ring.stop();
            }
        });
        alertDialog.setMessage("Incoming Booking from "+userName);
        alertDialog.show();

        //Draw polylines if the ambulance is booked
        if(bookingStatus) {
            //Retrieve points for drawing poly-lines on Map with updated ambulance location
            String url = getDirectionsUrl(bookedLocation, currentAmbulanceMarker.getPosition());
            AmbulanceMapsActivity.DownloadTask downloadTask = new AmbulanceMapsActivity.DownloadTask();
            //Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
    }

    //Method to get the booking status of current ambulance and draw poly lines
    public void getBookingStatus() {
        //Add listener to get booking status
        if(dutyStatus && userMarker == null) {
            currentAmbulanceDetails.child("bookingId").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (//dataSnapshot.hasChild("bookingId") && dataSnapshot.child("bookingId").getValue() != "None"
                            dataSnapshot.getValue() != "None" ) {
                        //Display user booked location marker on map
                        final String bookingId = (String) dataSnapshot.getValue();
                        //Toast.makeText(AmbulanceMapsActivity.this, ""+bookingId, Toast.LENGTH_SHORT).show();
                        DatabaseReference bookedUserDetails = firebaseDatabase.getReference().child("Users");
                        if(previousBookingId == null || !previousBookingId.equals(bookingId))
                        {
                            bookedUserDetails.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                        if (ds.getKey().equals(bookingId)) {
                                            bookingStatus = true;
                                            previousBookingId = bookingId;
                                            UserDetails bookedUserDetails = new UserDetails();
                                            bookedUserDetails.name = (String) ds.child("name").getValue();
                                            bookedClientName = (String) ds.child("name").getValue();
                                            bookedClientNumber = (Long) ds.child("phoneNumber").getValue();
                                            bookedUserDetails.latLng = new LatLng((double) ds.child("latLng").child("latitude").getValue(),
                                                    (double) ds.child("latLng").child("longitude").getValue());
                                            bookedLocation = bookedUserDetails.latLng;
                                            //Toast.makeText(AmbulanceMapsActivity.this, "BookedUserDetails: " + bookedUserDetails.latLng, Toast.LENGTH_SHORT).show();
                                            if (bookedUserDetails != null) {
                                                MarkerOptions bookedUserMarker = new MarkerOptions();
                                                bookedUserMarker.position(bookedUserDetails.latLng);
                                                bookedUserMarker.title(bookedUserDetails.name);
                                                userMarker = mapUserInterface.mMap.addMarker(bookedUserMarker);
                                                //Set up alert dialog to allow the driver to accept booking
                                                CustomDialog(bookedClientName);
                                            }

                                            //Set the details in bottom sheet
                                            custNameView.setText(bookedClientName);
                                            custPhoneView.setText(bookedClientNumber.toString());
                                            getDirections.setVisibility(View.VISIBLE);
                                            callClient.setVisibility(View.VISIBLE);
                                            getDirections.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    getBookedButton();
                                                }
                                            });
                                            callClient.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    getCallDriverID();
                                                }
                                            });
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }
                    else
                        bookingStatus = false;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        else if(cancelStatus){
            if(userMarker != null)
                userMarker.remove();
            //Set details in bottom sheet
            custNameView.setText("Booking Not Available");
            callClient.setVisibility(View.INVISIBLE);
            custPhoneView.setVisibility(View.INVISIBLE);
            getDirections.setVisibility(View.INVISIBLE);
        }
        //Add : Condition when ambulance has reached the booked location
    }

    public void setMarker() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(location.getLatitude(),location.getLongitude()));
                markerOptions.snippet("You");
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_amb));
                currentAmbulanceMarker = MapUserInterface.mMap.addMarker(markerOptions);
                mapUserInterface.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentAmbulanceMarker.getPosition(), 20));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AmbulanceMapsActivity.this, "Unable to get location", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ambulanceCanceledListener() {
        firebaseDatabase.getReference().child("Ambulance").child(FirebaseAuth.getInstance().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("bookingId") && dataSnapshot.child("boookingId") != null) {

                }
                else {
                    //Clear map
                    if(bookingStatus)
                    {
                        cancelStatus = true;
                        Toast.makeText(AmbulanceMapsActivity.this, "Booking Cancelled", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        bookingStatus = false;
                        userMarker = null;
                    }
                    mapUserInterface.mMap.clear();
                    setMarker();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Code to draw polyline on Map
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    // A method to download json data from url
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } catch (Exception e) {
            //Log.d("ExceptionDownloadingUrl", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                //Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            AmbulanceMapsActivity.ParserTask parserTask = new AmbulanceMapsActivity.ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    //A class to parse the Google Places in JSON format
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(Color.BLUE);
                Toast.makeText(AmbulanceMapsActivity.this, "Your destination is "
                        +duration + " away", Toast.LENGTH_SHORT).show();
            }

            //Toast.makeText(ClientMapsActivity.this, "Distance:" + distance + ", Duration:" + duration, Toast.LENGTH_SHORT).show();
            // Drawing polyline in the Google Map for the i-th route
            mapUserInterface.mMap.addPolyline(lineOptions);

        }
    }
}