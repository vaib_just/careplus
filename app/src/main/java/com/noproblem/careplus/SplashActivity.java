package com.noproblem.careplus;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.noproblem.careplus.Firebase.FirebaseAuthentication;

public class SplashActivity extends AppCompatActivity {

    private final int holdTimer = 3000;
    ImageView npLogo;
    TextView copyrightText;

    FirebaseAuthentication authentication;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseUser firebaseUser;
    FirebaseAuth firebaseAuth;
    boolean flag = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Error : Implement it afterwards
        /*if(requestCode == 0)
        {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.SEND_SMS)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},1);
                    }
                }
                if(permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {

                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);
                    }
                }
            }

            }*/
            if(requestCode == 2 && (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED))
                flag = true;
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        copyrightText = findViewById(R.id.copyright);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        copyrightText.setText(Html.fromHtml("&copy; No Problem <br>All Rights Reserved. 2018 <br>Crafted with &#10084;",
                Html.FROM_HTML_MODE_COMPACT));

        if (ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            //Error : App will run only when the user will provide both permissions
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_FINE_LOCATION},2);
        }
        else
        {
            flag = true;
        }

        if(flag)
            startLoginActivity();

    }

    @Override
    protected void onResume() {
        if(flag)
        {
            startLoginActivity();
        }
        else {
            Toast.makeText(this, "Please provide the permission required", Toast.LENGTH_SHORT).show();
            System.exit(0);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(authentication != null)
            authentication.detachListener();
    }

    public void startLoginActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                firebaseAuth = FirebaseAuth.getInstance();
                firebaseUser = firebaseAuth.getCurrentUser();
                if(firebaseUser != null)
                {
                    databaseReference = firebaseDatabase.getReference().child("Users");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(firebaseUser.getUid()))
                            {
                                Intent intent = new Intent(SplashActivity.this, ClientMapsActivity.class);
                                SplashActivity.this.startActivity(intent);
                                SplashActivity.this.finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    databaseReference = firebaseDatabase.getReference().child("Ambulance");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(firebaseUser.getUid()))
                            {
                                Intent intent = new Intent(SplashActivity.this, AmbulanceMapsActivity.class);
                                SplashActivity.this.startActivity(intent);
                                SplashActivity.this.finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                else
                {
                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                    authentication.attachListener();
                }
            }
        },holdTimer);

        authentication = new FirebaseAuthentication(this);
    }
}
