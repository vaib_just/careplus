package com.noproblem.careplus.Tools;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.noproblem.careplus.Fragments.AmbulanceSignUp;
import com.noproblem.careplus.Fragments.LoginFragment;
import com.noproblem.careplus.Fragments.ResetPasswordFragment;
import com.noproblem.careplus.Fragments.SignUpFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new LoginFragment();
            case 1:
                return new SignUpFragment();
            case 2:
                return new ResetPasswordFragment();
            case 3:
                return new AmbulanceSignUp();
            default:
                return null;
        }
    }



    @Override
    public int getCount() {
        return 4;
    }
}
