package com.noproblem.careplus.Tools;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.noproblem.careplus.ClientMapsActivity;
import com.noproblem.careplus.Databases.AmbulanceDetails;
import com.noproblem.careplus.Databases.OrgDesDetails;
import com.noproblem.careplus.Location.MapUserInterface;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.noproblem.careplus.Location.MapUserInterface.mMap;

public class DirectionCalculator{

    public String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        return  "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

    }

    /**
     * A method to download json data from url
     */
    private static String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //Log.d("ExceptionDownloadingUrl", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    public static class DownloadTask extends AsyncTask<String, Void, String> {

        public Context context;
        LatLng bookingLocation, ambulanceCurrentLocation;
        GoogleMap gMap;
        public DownloadTask(Context context,LatLng bookingLocation, LatLng ambulanceCurrentLocation, GoogleMap gMap) {
            this.context = context;
            this.bookingLocation = bookingLocation;
            this.ambulanceCurrentLocation = ambulanceCurrentLocation;
            this.gMap = gMap;
        }
        // Downloading data in non-ui thread
        @Override
        public String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                //Log.d("Background Task", e.toString());
            }
            return data;
        }
        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask(context,bookingLocation,ambulanceCurrentLocation,gMap);
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private static class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> > {

        double lat, lng;
        LatLng bookingLocation, ambulanceCurrentLocation;
        GoogleMap gMap;
        Context context;
        LatLng position;
        ArrayList<LatLng> points;
        String distance, duration;
        MarkerOptions userMarker = new MarkerOptions(), ambulanceLocation = new MarkerOptions();
        public ParserTask(Context context,LatLng bookingLocation, LatLng ambulanceCurrentLocation, GoogleMap gMap) {
            this.context = context;
            this.bookingLocation = bookingLocation;
            this.ambulanceCurrentLocation = ambulanceCurrentLocation;
            this.gMap = gMap;
        }

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        public void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            if (result.size() < 1) {
                //Toast.makeText(getBaseContext(), "No Route Found, Ambulance cannot be booked", Toast.LENGTH_SHORT).show();
                return;
            }
            else {

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        if (j == 0) {    // Get distance from the list
                            distance = point.get("distance");
                            continue;
                        } else if (j == 1) { // Get duration from the list
                            duration = point.get("duration");
                            continue;
                        }
                        lat = Double.parseDouble(point.get("lat"));
                        lng = Double.parseDouble(point.get("lng"));
                        position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    Toast.makeText(context, "Distance from function:" + distance, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context, "Distance from function:" + orgDesDetails.distance, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context, "Duration from function:" + orgDesDetails.duration, Toast.LENGTH_SHORT).show();
                    //Assign the respective distance, duration and polyLinePoints to AmbulanceDetails
                    //AmbulanceDetailsAndDistance ambulanceDetailsAndDistance = new AmbulanceDetailsAndDistance();
                    // Adding all the points in the route to LineOptions
            /*// Drawing polyline in the Google Map for the i-th route
            mapUserInterface.mMap.addPolyline(lineOptions);*/
                    //Add user's booking location marker
                    userMarker.position(bookingLocation);
                    userMarker.title(distance);
                    gMap.addMarker(userMarker);
                    PolylineOptions polylineOptions = new PolylineOptions();
                    polylineOptions.addAll(points);
                    gMap.addPolyline(polylineOptions);
                    //Add ambulance's marker
                    ambulanceLocation.position(ambulanceCurrentLocation);
                    ambulanceLocation.title("Ambulance");
                    gMap.addMarker(ambulanceLocation);
                }
            }
        }
    }
    private class AmbulanceDetailsAndDistance {
        AmbulanceDetails ambulanceDetails;
        //Nearest ambulance distance and points to draw polyline
        String ambulanceDistance, ambulanceDuration;
        ArrayList<LatLng> ambulancePoints;
        LatLng ambulancePosition;
        AmbulanceDetailsAndDistance(AmbulanceDetails ambulanceDetails, String ambulanceDistance, String ambulanceDuration, ArrayList<LatLng> ambulancePoints,
                                    LatLng ambulancePosition) {
            this.ambulanceDetails = ambulanceDetails;
            this.ambulanceDistance = ambulanceDistance;
            this.ambulanceDuration = ambulanceDuration;
            this.ambulancePoints = ambulancePoints;
            this.ambulancePosition = ambulancePosition;
        }
    }
}