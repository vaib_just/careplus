package com.noproblem.careplus.Tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.noproblem.careplus.Firebase.FirebaseAuthentication;

public class AlertDialogBox {

    private AlertDialog.Builder alertDialog;
    private Context context;

    public AlertDialogBox(Context context) {
        this.context = context;
        alertDialog = new AlertDialog.Builder(context);
    }

    public void emailVerifyDialog(final FirebaseAuthentication authentication) {
        alertDialog.setMessage("Signup Success. Please verify your email");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(!authentication.verifyEmail()) {
                    alertDialog.setMessage("Email not verified. Please verify first to start application!");
                    alertDialog.show();
                }
                else {
                    //Issue : Dialog box opens twice after the email is verified
                }
            }
        });
        alertDialog.show();
    }

    //Add : Use these while user books the ambulance to verify their email
    /*public void emailVerifyDialogOnStart(final FirebaseAuthentication authentication) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null && !authentication.verifyEmail()) {
            alertDialog.setMessage("Email not verified. Please verify first to start application!");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            alertDialog.show();
        }
        else if(FirebaseAuth.getInstance().getCurrentUser()!=null && authentication.verifyEmail()) {
            authentication.attachListener();
        }
    }

    public void emailVerifyDialogOnStart() {
            alertDialog.setMessage("Email not verified. Please verify first to start application!");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            alertDialog.show();
         }
         */

    //Create dialog with no actions but only print a msg
    public void noActionDialog(String msg) {
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alertDialog.show();
    }

}
