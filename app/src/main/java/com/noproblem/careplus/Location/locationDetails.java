package com.noproblem.careplus.Location;

public class locationDetails {

    private double Latitude;
    private double Longitude;

    public locationDetails(double latitude, double longitude) {
        Latitude = latitude;
        Longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }
}
