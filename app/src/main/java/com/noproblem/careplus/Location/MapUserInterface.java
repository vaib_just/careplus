package com.noproblem.careplus.Location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.google.android.gms.maps.GoogleMap;

public class MapUserInterface extends AppCompatActivity {

    public static GoogleMap mMap;
    public static boolean mLocationPermissionGranted = false;
    public static Location mLastKnownLocation;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static LocationManager locationManager;
    public static ConnectivityManager connManager;
    public static NetworkInfo mWifi;
    public static NetworkInfo mInternet;
    public static boolean gps_enabled = false;
    public static boolean network_enabled = false;
    public static boolean wifiEnabled;
    public static boolean internetEnabled;


    public static boolean getCallingPermission(Context context){

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE},0);
        }

        return true;
    }
    public static boolean getLocationPermission(Context context) {

       locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            } else {
                mLocationPermissionGranted = true;
            }

        }
        return true;
    }

    //Check GPS and Network connection and open concerned settings
    public void checkNetworkAndGPSConnectivity(Context context) {
        //Check GPS connectivity
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (NullPointerException ex) {
            Toast.makeText(this, "Exception: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        //If GPS is not activated, open settings
        if (!gps_enabled) {
            //Create dialog to prompt user to turn on GPS and on OKAY button open locational settings

            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(myIntent);
        }

        //Check Network connectivity
        connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        try {
            mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            mInternet = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        } catch (NullPointerException nPE) {
            // Null Pointer Exception
        }
        wifiEnabled = mWifi != null && mWifi.isConnected();
        internetEnabled = mInternet != null && mInternet.isConnected();
        network_enabled = wifiEnabled || internetEnabled;
        //If net is not activated, open settings
        if (!network_enabled) {
            //Create dialog to prompt user to turn on the internet
        }
    }

    public static void updateLocationUI() {

            try {
                if (!mLocationPermissionGranted) {
                    mMap.setMyLocationEnabled(false);
                    mMap.setIndoorEnabled(false); // only runs on routes
                    mMap.setTrafficEnabled(false); // hides traffic
                    mMap.setBuildingsEnabled(false); //shows building
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    mMap.getUiSettings().setZoomControlsEnabled(false); // user will be able to control Zoom In/Out.
                    mLastKnownLocation = null;

                } else {
                    mMap.setMyLocationEnabled(true);
                    mMap.setIndoorEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true); // user won't be able tp control Zoom Settings.
                    mMap.getUiSettings().setZoomControlsEnabled(false);
                    mMap.setTrafficEnabled(true);
                    mMap.setBuildingsEnabled(false);


                }
            } catch (SecurityException e) {

        }
    }

}
