package com.noproblem.careplus.customfonts;

import android.content.Context;
import android.graphics.Typeface;

public class FontConstants {

    /* Getting Fonts From Assests */

    public static final String FONT_BOLD = "fonts/MavenPro-Bold.ttf";
    public static final String FONT_NORMAL = "fonts/MavenPro-Regular.ttf";
    public static final String FONT_MEDIUM = "fonts/MavenPro-Medium.ttf";

    public static Typeface getfontNormal(Context context){
        return Typeface.createFromAsset(context.getAssets(),FontConstants.FONT_NORMAL);
    }

    public static Typeface getfontBold(Context context){
        return  Typeface.createFromAsset(context.getAssets(),FontConstants.FONT_BOLD);
    }
}
