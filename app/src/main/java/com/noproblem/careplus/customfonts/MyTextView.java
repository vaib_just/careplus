package com.noproblem.careplus.customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MyTextView extends android.support.v7.widget.AppCompatTextView {

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        // If TextView Isn't focused then change font
        if (!isInEditMode()) {
            Typeface tf;
            switch (getTypeface().getStyle()) {
                case Typeface.BOLD:
                    tf=Typeface.createFromAsset(getContext().getAssets(), FontConstants.FONT_BOLD);
                    break;
                default:
                    tf=Typeface.createFromAsset(getContext().getAssets(), FontConstants.FONT_NORMAL);
            }
            setTypeface(tf);
        }
    }
}
