package com.noproblem.careplus.Fragments;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;
import com.noproblem.careplus.Firebase.FirebaseAuthentication;
import com.noproblem.careplus.R;
import com.noproblem.careplus.Tools.AlertDialogBox;
import com.noproblem.careplus.Tools.MyTimeUtils;
import com.noproblem.careplus.customfonts.MyEditText;
import java.util.Calendar;
import java.util.Date;

import static com.noproblem.careplus.Tools.StringValidation.isValidEmail;
import static com.noproblem.careplus.Tools.StringValidation.isValidName;
import static com.noproblem.careplus.Tools.StringValidation.isValidPassword;


public class SignUpFragment extends Fragment {
    MyEditText nameText,emailText,passwordText, dataText;
    Date dob;
    FirebaseAuthentication authentication;

    //Alert Dialog for email verification
    AlertDialogBox alertDialog;


    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().findViewById(R.id.clickable_birthday_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                birthDayClick();
            }
        });
        getView().findViewById(R.id.signup_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameText = getView().findViewById(R.id.username_edittext);
                emailText = getView().findViewById(R.id.email_edittext);
                passwordText = getView().findViewById(R.id.password_edittext);
                dataText = getView().findViewById(R.id.birthday_edittext);
                alertDialog = new AlertDialogBox(view.getContext());
                authentication = new FirebaseAuthentication(view.getContext());
                authentication.attachListener();

                String name = nameText.getText().toString();
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();

                boolean ok = true;
                if (!isValidName(name)) {
                    ((MyEditText) getView().findViewById(R.id.username_edittext)).setError("Enter a Valid Name");
                    ok = false;
                }
                if (!isValidEmail(email)) {
                    ((MyEditText) getView().findViewById(R.id.email_edittext)).setError("Invalid Email");
                    ok = false;
                }
                if (!isValidPassword(password)) {
                    ((MyEditText) getView().findViewById(R.id.password_edittext)).setError("Password Must Contain Small,Caps,Special,Numbers");
                    ok = false;
                }
                if (ok) {
                    authentication.createUserWithEmailAndPassword(name,password,email,dob);
                }

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(authentication != null)
            authentication.detachListener();
    }

    public void birthDayClick() {
        final Calendar calendar = Calendar.getInstance();
        final int allowedDOB = 10;

        // calendar will show date of last 14 Years from current date
        calendar.add(Calendar.YEAR,-10);
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        calendar.set(year, monthOfYear, dayOfMonth);
                        long selectDateInMilliSeconds = calendar.getTimeInMillis();

                        Calendar currentDate = Calendar.getInstance();
                        long currentDateInMilliSeconds = currentDate.getTimeInMillis();

                        long diffDate = currentDateInMilliSeconds - selectDateInMilliSeconds;
                        Calendar yourAge = Calendar.getInstance();

                        yourAge.setTimeInMillis(diffDate);

                        long returnedYear = yourAge.get(Calendar.YEAR) - 1970;
                        yourAge.set(Calendar.YEAR, year);
                        yourAge.set(Calendar.MONTH, monthOfYear);
                        yourAge.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        if (returnedYear < allowedDOB) {
                           Toast.makeText(getActivity(), "Sorry!!! You must be at least 14 Years old to use this app ", Toast.LENGTH_LONG).show();
                            ((MyEditText) getView().findViewById(R.id.birthday_edittext)).setError("You Must Be 14 Years OLD");
                            return;
                        }
                        else if (selectDateInMilliSeconds > currentDateInMilliSeconds) {
                           Toast.makeText(getActivity(), "Your birthday date must come before today's date", Toast.LENGTH_LONG).show();
                            ((MyEditText) getView().findViewById(R.id.birthday_edittext)).setError("Invalid Year");
                            return;
                        } else {

                            ((MyEditText) getView().findViewById(R.id.birthday_edittext)).setText(MyTimeUtils.formatDate(yourAge.getTimeInMillis(), MyTimeUtils.BIRTHDAY_FORMAT));
                            dob = yourAge.getTime();
                        }


                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dpd.show();
    }
}
