package com.noproblem.careplus.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.noproblem.careplus.Firebase.FirebaseAuthentication;
import com.noproblem.careplus.R;
import com.noproblem.careplus.customfonts.MyEditText;

import static com.noproblem.careplus.Tools.StringValidation.isValidEmail;
import static com.noproblem.careplus.Tools.StringValidation.isValidPassword;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {


    MyEditText emailText,passwordText;
    FirebaseAuthentication authentication;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                emailText = getView().findViewById(R.id.email_edittext);
                passwordText = getView().findViewById(R.id.password_edittext);
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();
                authentication = new FirebaseAuthentication(view.getContext());

                boolean ok = true;

                // Validating Email From Tools.StringValidation.java
                if (!isValidEmail(email)) {
                    // MyEditText ID
                    ((MyEditText) getView().findViewById(R.id.email_edittext)).setError("Invalid Email");
                    ok = false;
                }
                // Validating Email From Tools.StringValidation.java
                if (!isValidPassword(password)) {
                    ((MyEditText) getView().findViewById(R.id.password_edittext)).setError("Invalid Password");
                    ok = false;
                }
                if (ok) {
                    authentication.signInUsingEmail(email,password);
                }

            }
        });
    }
}
