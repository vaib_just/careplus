package com.noproblem.careplus.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.noproblem.careplus.Firebase.FirebaseAuthentication;
import com.noproblem.careplus.R;
import com.noproblem.careplus.customfonts.MyEditText;
import static com.noproblem.careplus.Tools.StringValidation.isValidEmail;

public class ResetPasswordFragment extends Fragment {

    MyEditText emailText;
    FirebaseAuthentication authentication;

    public ResetPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reset_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        authentication = new FirebaseAuthentication(view.getContext());
        getView().findViewById(R.id.reset_password_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                emailText = getView().findViewById(R.id.email_edittext);
                String email = emailText.getText().toString();
                boolean ok = true;
                if (!isValidEmail(email)) {
                    ((MyEditText) getView().findViewById(R.id.email_edittext)).setError("Invalid Email");
                    ok = false;
                }
                if (ok)
                    authentication.resetPassword(email);
            }
        });
    }
}
