package com.noproblem.careplus.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.noproblem.careplus.Firebase.FirebaseAuthentication;
import com.noproblem.careplus.R;
import com.noproblem.careplus.Tools.AlertDialogBox;
import com.noproblem.careplus.customfonts.MyEditText;
import static com.noproblem.careplus.Tools.StringValidation.isValidEmail;
import static com.noproblem.careplus.Tools.StringValidation.isValidPassword;


public class AmbulanceSignUp extends Fragment {
    MyEditText hospitalText,contactText, emailText,passwordText, licensePlateNoText, typeText;
    FirebaseAuthentication authentication;

    //Alert Dialog for email verification
    AlertDialogBox alertDialog;


    public AmbulanceSignUp() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ambulance_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().findViewById(R.id.signup_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hospitalText = getView().findViewById(R.id.hospitalName_edittext);
                contactText = getView().findViewById(R.id.contact_edittext);
                emailText = getView().findViewById(R.id.email_edittext);
                passwordText = getView().findViewById(R.id.password_edittext);
                licensePlateNoText = getView().findViewById(R.id.licensePlateNo_edittext);
                typeText = getView().findViewById(R.id.type_edittext);
                alertDialog = new AlertDialogBox(view.getContext());
                authentication = new FirebaseAuthentication(view.getContext());
                authentication.attachListener();

                String hospitalRegNum = hospitalText.getText().toString();
                String contact = contactText.getText().toString();
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();
                String licensePlateNo = licensePlateNoText.getText().toString();
                String type = typeText.getText().toString();
                boolean ok = true;
                if (hospitalRegNum.isEmpty()) {
                    ((MyEditText) getView().findViewById(R.id.hospitalName_edittext)).setError(getString(R.string.empty));
                    ok = false;
                }
                if(contact.isEmpty()) {
                    ((MyEditText) getView().findViewById(R.id.contact_edittext)).setError("Required");
                    ok = false;
                }
                if (!isValidEmail(email)) {
                    ((MyEditText) getView().findViewById(R.id.email_edittext)).setError("Invalid Email");
                    ok = false;
                }
                if (!isValidPassword(password)) {
                    ((MyEditText) getView().findViewById(R.id.password_edittext)).setError("Invalid Password");
                    ok = false;
                }
                if(licensePlateNo.isEmpty())
                {
                    ((MyEditText) getView().findViewById(R.id.licensePlateNo_edittext)).setError("Required");
                    ok = false;
                }
                if(type.isEmpty()) {
                    ((MyEditText) getView().findViewById(R.id.type_edittext)).setError("Required");
                    ok = false;
                }
                if (ok) {
                    authentication.createAmbulanceWithEmailAndPassword(hospitalRegNum, Long.parseLong(contact),
                            email,password,licensePlateNo,type);
                }

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(authentication != null)
            authentication.detachListener();
    }
}
