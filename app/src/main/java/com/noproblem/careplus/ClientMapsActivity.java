package com.noproblem.careplus;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.*;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.noproblem.careplus.Databases.AmbulanceDetails;
import com.noproblem.careplus.Location.MapUserInterface;
import com.noproblem.careplus.Tools.DirectionCalculator;
import com.noproblem.careplus.Tools.DirectionsJSONParser;
import android.support.design.widget.*;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import static android.content.Intent.ACTION_CALL;
import static com.noproblem.careplus.Location.MapUserInterface.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
import static com.noproblem.careplus.Location.MapUserInterface.mMap;

public class ClientMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    SupportMapFragment mapFragment;
    public Location mLastKnownLocation;
    public static final int DEFAULT_ZOOM = 15;
    private MapUserInterface mapUserInterface;
    private Button book;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Marker bookingMarker;
    private LatLng bookingLocation;
    private boolean bookingStatus = false;

    //Firebase Variables to write data into user's database
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference userBookingLocation;

    //Database reference to get the data of all ambulances available
    private DatabaseReference noOfAmbulances;
    ArrayList<AmbulanceDetails> ambulanceMapDetails;

    DirectionCalculator directionCalculator;
    ValueEventListener noOfAmbListener;
    AmbulanceDetails nearestAmbulanceDetails;

    Button usersignOut;

    Button callDriverID, cancelAmbulance;
    TextView driverNameView, driverPhoneView;
    TextView hospitalNameView, ambulanceLicenseView, hospitalAddressView;
    String phoneNumber;


    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.ClientMap);

        driverNameView = findViewById(R.id.driverName);
        driverPhoneView = findViewById(R.id.driverPhone);
        hospitalNameView = findViewById(R.id.hospitalName);
        ambulanceLicenseView = findViewById(R.id.ambulanceLicense);
        hospitalAddressView = findViewById(R.id.hospitalAddress);
        callDriverID = findViewById(R.id.helloDriver);
        cancelAmbulance = findViewById(R.id.cancelBooking);
        ButterKnife.bind(this);

        // This Hide the Bottom Sheet By default
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setHideable(false);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING){
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        book = findViewById(R.id.bookButton);
        usersignOut = findViewById(R.id.signOut);
        mapUserInterface = new MapUserInterface();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        final Marker[] ambMarker = {null};
        //Listener to display active ambulance on the map with hospital Name
        noOfAmbListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    //Add : Set duty to on and driverName != null
                    if (snapshot.hasChild("location") &&
                            Boolean.parseBoolean(snapshot.child("duty").getValue().toString())
                            && !snapshot.child("driverName").getValue().toString().equals("None")) {
                        layoutBottomSheet.setVisibility(View.VISIBLE);
                        String id = snapshot.getKey();
                        boolean exists = false;
                        //Check if ambulance marker is already marked
                        for(AmbulanceDetails ambulanceDetails : ambulanceMapDetails) {
                            if(ambulanceDetails.id.equals(id))
                                exists = true;
                            else{ }
                            //Add : If ambulance already exists calculate the index of marker
                        }
                        if(exists)
                        {//Toast.makeText(ClientMapsActivity.this, "Ambulance already exists", Toast.LENGTH_SHORT).show();
                        //Add : If ambulance already exists, change the position of marker
                        }
                        // else if(exists && )
                        else {
                            long contact = Long.parseLong(snapshot.child("contact").getValue().toString());
                            String hospitalRegNum = snapshot.child("hospitalRegNum").getValue().toString();
                            String licensePlateNo = snapshot.child("licensePlateNo").getValue().toString();
                            String type = snapshot.child("type").getValue().toString();
                            boolean duty = Boolean.parseBoolean(snapshot.child("duty").getValue().toString());
                            String driverName = snapshot.child("driverName").getValue().toString();
                            double latitude = Double.parseDouble(snapshot.child("location").child("latitude").getValue().toString());
                            double longitude = Double.parseDouble(snapshot.child("location").child("longitude").getValue().toString());
                            LatLng location = new LatLng(latitude,longitude);
                            AmbulanceDetails ambulanceDetails = new AmbulanceDetails(id,contact,hospitalRegNum,licensePlateNo,type,duty,driverName,location);
                            ambulanceMapDetails.add(ambulanceDetails);
                            DataSnapshot locSnapshot = snapshot.child("location");
                            if (locSnapshot.exists()) {
                                MarkerOptions markerOptions = new MarkerOptions();
                                //markerOptions.title(hospital);
                                markerOptions.position(location);
                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_amb));
                                ambMarker[0] = mMap.addMarker(markerOptions);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        //When no ambulance is available
        if(ambMarker[0] == null) {
            Toast.makeText(ClientMapsActivity.this, "Sorry, no active ambulances available right now",
                    Toast.LENGTH_SHORT).show();
            layoutBottomSheet.setVisibility(View.INVISIBLE);
         //   return;
        }
        //Get instances of firebase database
        firebaseDatabase = FirebaseDatabase.getInstance();
        userBookingLocation = firebaseDatabase.getReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().
                getUid()).child("latLng");
        noOfAmbulances = firebaseDatabase.getReference().child("Ambulance");
        ambulanceMapDetails = new ArrayList<>();
        directionCalculator = new DirectionCalculator();

        mapFragment.getMapAsync(this);
        MapUserInterface mapUserInterface = new MapUserInterface();
        mapUserInterface.getLocationPermission(this);
        mapUserInterface.checkNetworkAndGPSConnectivity(this);

        getCallDriverID();
        getUserSignOut();
        getCancelAmbulance();


    }


    public void getUserSignOut() {

        usersignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(ClientMapsActivity.this,LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                startActivity(intent);
                finish();
            }
        });
    }

    //Method to get current location and set the marker
    public void getDeviceLocation() {

        if (MapUserInterface.getLocationPermission(this)) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
            //Add : RequestForPermissionResult Callback to check whether permission are granted or not
        }

        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLastKnownLocation = location;
                    bookingLocation = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(bookingLocation.latitude, bookingLocation.longitude), DEFAULT_ZOOM));
                    bookingMarker = mapUserInterface.mMap.addMarker(new MarkerOptions().position(bookingLocation));
                } else {
                    //Add : Location can't be traced
                    Toast.makeText(ClientMapsActivity.this, "Unable to location", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final String TAG = ClientMapsActivity.class.getSimpleName();
        mapUserInterface.mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // Find the JSON File in a raw resource file.
            // Dark Themed Style | Can Be Customized Easily by visiting: https://mapstyle.withgoogle.com/
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));
            mapUserInterface.updateLocationUI();

            if (!success) {
                //Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            //Log.e(TAG, "Can't find style. Error: ", e);
        }

        mapUserInterface.mMap.setTrafficEnabled(false);
        getDeviceLocation();

        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING){
                            sheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {


                    }
                });
                if (ActivityCompat.checkSelfPermission(ClientMapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(ClientMapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mapUserInterface.mMap.setMyLocationEnabled(false);
                book.setVisibility(View.GONE);
                bookingMarker.setDraggable(false);
                bookingStatus = true;
                //Send Client Location to firebase and notify driver
                userBookingLocation.setValue(bookingLocation);
                nearestAmbulanceBook(bookingLocation);

            }
        });

        mapUserInterface.mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (!bookingStatus)
                {
                    bookingLocation = latLng;
                    bookingMarker.setPosition(bookingLocation);
                    mapUserInterface.mMap.animateCamera(CameraUpdateFactory.newLatLng(bookingMarker.getPosition()));
                }
            }
        });

        //Display active ambulance on the map with hospital Name
        noOfAmbulances.addValueEventListener(noOfAmbListener);
    }


    //Method to check the nearest Ambulance
    public void nearestAmbulanceBook(LatLng origin) {
        ArrayList<AmbulanceDetailsAndDistance> ambulanceDetailsAndDistances = new ArrayList<>();
        AmbulanceDetailsAndDistance detailsAndDistances;
        for (AmbulanceDetails ambulanceDetails : ambulanceMapDetails) {
            float[] results = new float[1];
            LatLng destination = ambulanceDetails.getLocation();
            Location.distanceBetween(origin.latitude, origin.longitude, destination.latitude, destination.longitude, results);
            detailsAndDistances = new AmbulanceDetailsAndDistance();
            detailsAndDistances.ambulanceDetails = ambulanceDetails;
            detailsAndDistances.ambulanceDistance = results[0];
            ambulanceDetailsAndDistances.add(detailsAndDistances);
            //Toast.makeText(this, "Distance from main: "+results[0], Toast.LENGTH_SHORT).show();
        }
        if (ambulanceDetailsAndDistances.size() > 0) {
            float shortestDistance = ambulanceDetailsAndDistances.get(0).ambulanceDistance;
            nearestAmbulanceDetails = ambulanceDetailsAndDistances.get(0).ambulanceDetails;
            //Loop to check which ambulance is nearest to the booking location
            for (AmbulanceDetailsAndDistance ambulanceDetailsDistance : ambulanceDetailsAndDistances) {
                if (shortestDistance > ambulanceDetailsDistance.ambulanceDistance) {
                    nearestAmbulanceDetails = ambulanceDetailsDistance.ambulanceDetails;
                    shortestDistance = ambulanceDetailsDistance.ambulanceDistance;
                }
            }


            //Check if the ambulance is in range of 5 kms or not
            if(shortestDistance > 5000) {
                Toast.makeText(this, "Sorry no ambulance found in your location", Toast.LENGTH_SHORT).show();
                return;
            }
            //Assign the values to the bottom sheet
            final DatabaseReference databaseReference = firebaseDatabase.getReference();
            databaseReference.child("Ambulance").child(nearestAmbulanceDetails.id).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            driverNameView.setText(dataSnapshot.child("driverName").getValue(String.class));
                            phoneNumber = dataSnapshot.child("contact").getValue().toString();
                            driverPhoneView.setText(phoneNumber);
                            ambulanceLicenseView.setText(dataSnapshot.child("licensePlateNo").getValue(String.class));
                            //To get the value from hospital database
                            DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference().child("Hospitals").child(nearestAmbulanceDetails.hospitalRegNum);
                            databaseReference1.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    hospitalNameView.setText(dataSnapshot.child("hospitalName").getValue(String.class));
                                    hospitalAddressView.setText(dataSnapshot.child("address").getValue(String.class));
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );

            //Add : For updating the polyline live using fire-base : If location of ambulance is changed then track it using fire-base

            //Assign listener to booked ambulance's database and track if location of ambulance is changed or not
            databaseReference.child("Ambulance").child(nearestAmbulanceDetails.id).addChildEventListener(new ChildEventListener() {
                  @Override
                  public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                      if (dataSnapshot.child("latitude").getValue() != null) {
                          LatLng ambulanceCurrentLocation = new LatLng(Double.parseDouble(dataSnapshot.child("latitude").getValue().toString())
                                  , Double.parseDouble(dataSnapshot.child("longitude").getValue().toString()));
                          //Retrieve points for drawing poly-lines on Map with updated ambulance location
                          String url = getDirectionsUrl(bookingLocation, ambulanceCurrentLocation);
                          DownloadTask downloadTask = new DownloadTask();
                          //Start downloading json data from Google Directions API
                          downloadTask.execute(url);
                          } else {
                          //Toast.makeText(ClientMapsActivity.this, "Location doesn't exist", Toast.LENGTH_SHORT).show();
                      }
                      }
                      @Override
                      public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                      if (dataSnapshot.child("latitude").getValue() != null) {
                          LatLng ambulanceCurrentLocation = new LatLng(Double.parseDouble(dataSnapshot.child("latitude").getValue().toString())
                                  , Double.parseDouble(dataSnapshot.child("longitude").getValue().toString()));
                          //Retrieve points for drawing poly-lines on Map with updated ambulance location
                          String url = getDirectionsUrl(bookingLocation, ambulanceCurrentLocation);
                          DownloadTask downloadTask = new DownloadTask();
                          //Start downloading json data from Google Directions API
                          downloadTask.execute(url);
                          } else {
                          //Toast.makeText(ClientMapsActivity.this, "Location doesn't exist", Toast.LENGTH_SHORT).show();
                      }
                      }
                      @Override
                      public void onChildRemoved(DataSnapshot dataSnapshot) {
                      }
                      @Override
                      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                      }
                      @Override
                      public void onCancelled(DatabaseError databaseError) {
                      }
                      }
            );
            //Notify ambulance about booking
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                databaseReference.child("Ambulance").child(String.valueOf(nearestAmbulanceDetails.id)).child("bookingId")
                        .setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
            }
            else {

            }
        }
    }


    //Class to store Ambulance Details with its distance from the booked point
    public class AmbulanceDetailsAndDistance {
        AmbulanceDetails ambulanceDetails;
        //Nearest ambulance distance and points to draw polyline
        float ambulanceDistance;
        String ambulanceDuration;
        ArrayList<LatLng> ambulancePoints;
        LatLng ambulancePosition;

        public AmbulanceDetailsAndDistance(AmbulanceDetails ambulanceDetails, float ambulanceDistance, String ambulanceDuration, ArrayList<LatLng> ambulancePoints,
                                           LatLng ambulancePosition) {
            this.ambulanceDetails = ambulanceDetails;
            this.ambulanceDistance = ambulanceDistance;
            this.ambulanceDuration = ambulanceDuration;
            this.ambulancePoints = ambulancePoints;
            this.ambulancePosition = ambulancePosition;
        }

        public AmbulanceDetailsAndDistance() {
        }
    }


    // DriverCall Button
    public void getCallDriverID() {

        callDriverID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(ACTION_CALL);
                if(phoneNumber != null)
                {
                    callIntent.setData(Uri.parse("tel:"+phoneNumber));
                    startActivity(callIntent);
                    Toast.makeText(ClientMapsActivity.this, "Calling Ambulance", Toast.LENGTH_SHORT).show();
                }
                if (ActivityCompat.checkSelfPermission(ClientMapsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(ClientMapsActivity.this, new String[]{Manifest.permission.CALL_PHONE},0);

                }
            }
        });
    }


    //Code to draw polyline on Map
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }


    // A method to download json data from url
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //Log.d("ExceptionDownloadingUrl", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                //Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }


    //A class to parse the Google Places in JSON format
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";
            if (result.size() < 1) {
                Toast.makeText(getBaseContext(), "Sorry, no ambulance found at your location", Toast.LENGTH_SHORT).show();
                return;
            }


            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(15);
                    lineOptions.color(Color.BLUE);
                    Toast.makeText(ClientMapsActivity.this, "Your ambulance is arriving in "
                            +duration, Toast.LENGTH_SHORT).show();
            }

            //Toast.makeText(ClientMapsActivity.this, "Distance:" + distance + ", Duration:" + duration, Toast.LENGTH_SHORT).show();
            // Drawing polyline in the Google Map for the i-th route
            mapUserInterface.mMap.addPolyline(lineOptions);

            }
    }

    //Method to cancel ambulance
    public void getCancelAmbulance() {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        cancelAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.setMessage("Are you sure you want to cancel the ambulance?");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(ClientMapsActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
                        mapUserInterface.mMap.clear();
                        mapUserInterface = new MapUserInterface();
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        book.setVisibility(View.VISIBLE);
                        bookingStatus = false;
                        mapFragment.getMapAsync(ClientMapsActivity.this);
                        //Update booking canceled details to firebase
                        FirebaseDatabase.getInstance().getReference().child("Ambulance").
                        child(nearestAmbulanceDetails.id).child("bookingId").setValue(null);
                        //Display active ambulance on the map with hospital Name
                        noOfAmbulances.addListenerForSingleValueEvent(noOfAmbListener);
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.show();
            }
        });
    }
}
