package com.noproblem.careplus.Firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.noproblem.careplus.AmbulanceMapsActivity;
import com.noproblem.careplus.ClientMapsActivity;
import com.noproblem.careplus.Databases.AmbulanceDetails;
import com.noproblem.careplus.Databases.UserDetails;
import com.noproblem.careplus.Tools.AlertDialogBox;

import java.util.Date;

public class FirebaseAuthentication {

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;

    //Firebase database instance variables
    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference;
    //Firebase AuthListener
    private FirebaseAuth.AuthStateListener authStateListener;

    boolean flag = false;

    //User details
    private String name;
    private Date dob;

    //Activity or fragment context
    private Context context;

    //Alert Dialog for email verification
    private AlertDialogBox alertDialog;

    //Constructor
    public FirebaseAuthentication(Context context) {
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();
        alertDialog = new AlertDialogBox(context);
    }

    //Method of Signing up using email and password
    public  void createUserWithEmailAndPassword(final String name,final String pass,final String email,final Date dob){

        this.name = name;
        this.dob = dob;

        //Create an OnSucessListener
        OnSuccessListener<AuthResult> successListener = new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                firebaseUser = mAuth.getCurrentUser();
                sendEmailVerification();
                databaseReference = firebaseDatabase.getReference().child("Users").child(firebaseUser.getUid());
                UserDetails userDetails = new UserDetails(name,firebaseUser.getEmail(),dob);
                databaseReference.setValue(userDetails);
            }
        };

        //Method to Create an OnFailureListener
        OnFailureListener failureListener = new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Log.d("Exception creating user"," : " +e.getMessage());
                if (e.getMessage().contains("network"))
                    alertDialog.noActionDialog("Cannot connect to internet. Please check you network settings.");
                if (e.getMessage().contains("email address is already in use"))
                {
                    //Add : If user already exists but is not verified
                    alertDialog.noActionDialog("Email already exists, please use another");
                }
            }
        };

        //Create new user
        mAuth.createUserWithEmailAndPassword(email,pass).addOnSuccessListener(successListener)
                .addOnFailureListener(failureListener);
    }

    //Method of Signing up the ambulance using email and password
    public  void createAmbulanceWithEmailAndPassword(final String hospitalRegNum, final long contact,
                                                     final String email, final String pass,
                                                     final String licensePlateNo, final String type){

        //Create an OnSucessListener
        final OnSuccessListener<AuthResult> successListener = new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                firebaseUser = mAuth.getCurrentUser();
                sendEmailVerification();
                databaseReference = firebaseDatabase.getReference().child("Ambulance").child(firebaseUser.getUid());
                AmbulanceDetails ambulanceDetails = new AmbulanceDetails(hospitalRegNum, contact,email,licensePlateNo,type);
                databaseReference.setValue(ambulanceDetails);
            }
        };

        //Method to Create an OnFailureListener
        final OnFailureListener failureListener = new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Log.d("Exception creating user"," : " +e.getMessage());
                if (e.getMessage().contains("network"))
                    alertDialog.noActionDialog("Cannot connect to internet. Please check you network settings.");
                if (e.getMessage().contains("email address is already in use"))
                {
                    //Add : If user already exists but is not verified
                    alertDialog.noActionDialog("Email already exists, please use another");
                }
            }
        };
        final boolean[] flag = {false};
        DatabaseReference hospitalReference = FirebaseDatabase.getInstance().getReference().child("Hospitals");
        hospitalReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if(snapshot.hasChild("registrationNumber") &&
                            snapshot.child("registrationNumber").getValue().toString().equals(hospitalRegNum))
                    {
                        mAuth.createUserWithEmailAndPassword(email,pass).addOnSuccessListener(successListener).
                                addOnFailureListener(failureListener);
                        flag[0] = true;
                    }
                }
                if(!flag[0])
                    Toast.makeText(context, "No such hospital exists", Toast.LENGTH_SHORT).show();
                }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Method to Send Email verification
    private void sendEmailVerification()
    {
        OnSuccessListener<Void> successListener = new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Error : Dialog box doesn't appear as getCurrentUser() triggers listener and opens map
                alertDialog.noActionDialog("Verification email sent. Please verify to use the app.");
                firebaseUser = mAuth.getCurrentUser();
            }
        };

        OnFailureListener failureListener = new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e.getMessage().contains("email exists"))
                    alertDialog.noActionDialog("Email already exists, please use another");
                else if(e.getMessage().contains("network"))
                    alertDialog.noActionDialog("Can't connect to internet. Please check you network settings.");
            }
        };
        firebaseUser.sendEmailVerification().addOnSuccessListener(successListener)
                .addOnFailureListener(failureListener);
    }

    //Method to check if email is verified or not
    public boolean verifyEmail() {
        if(firebaseUser != null) {
            firebaseUser.reload().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    firebaseUser = mAuth.getCurrentUser();
                    if(firebaseUser != null) {
                        if(firebaseUser.isEmailVerified()) {
                            //Store details in firebase database
                            databaseReference = firebaseDatabase.getReference().child("Users").child(firebaseUser.getUid());
                            UserDetails userDetails = new UserDetails(name,firebaseUser.getEmail(),dob);
                            databaseReference.setValue(userDetails);
                            flag = true;
                            Intent intent = new Intent(context.getApplicationContext(), ClientMapsActivity.class);
                            context.startActivity(intent);
                            ((Activity)context).finish();
                        }
                        else {
                        }
                    }
                    else {
                        alertDialog.noActionDialog("Successfully signed out");
                    }
                }
            });
        }
        return flag;
    }

    //Attach firebase auth state listener
    public void attachListener() {
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseAuth = FirebaseAuth.getInstance();
                firebaseUser = firebaseAuth.getCurrentUser();
                if(firebaseUser != null)
                {
                    databaseReference = firebaseDatabase.getReference().child("Users");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(firebaseUser.getUid()))
                            {
                                Intent intent = new Intent(context.getApplicationContext(), ClientMapsActivity.class);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });databaseReference = firebaseDatabase.getReference().child("Ambulance");
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(firebaseUser.getUid()))
                            {
                                Intent intent = new Intent(context.getApplicationContext(), AmbulanceMapsActivity.class);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        };
        mAuth.addAuthStateListener(authStateListener);
    }

    //Detach firebase auth state listener
    public void detachListener() {
        mAuth.removeAuthStateListener(authStateListener);
    }

    //Method to SignIn with email and password
    public void signInUsingEmail(String email, String pass) {

        OnSuccessListener<AuthResult> onSuccessListener = new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                firebaseUser = mAuth.getCurrentUser();
            }
        };

        OnFailureListener onFailureListener = new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context,"Sign in Failed",Toast.LENGTH_SHORT).show();
                //Log.d("Exception in SignIn: " , ""+e.getMessage());
                if(e.getMessage().contains("no user record"))
                    alertDialog.noActionDialog("Email is not registered, please sign up first.");
                else if(e.getMessage().contains("password is invalid"))
                    alertDialog.noActionDialog("You've entered a wrong password.");
                else if(e.getMessage().contains("network"))
                    alertDialog.noActionDialog("Can't connect to internet. Please check you network settings.");
            }
        };

        mAuth.signInWithEmailAndPassword(email,pass).addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);
    }

    //Method to reset password
    public void resetPassword(String email) {
        OnSuccessListener<Void> onSuccessListener = new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                alertDialog.noActionDialog("Reset password link sent. Please check registered email for details.");
            }
        };

        OnFailureListener onFailureListener = new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e.getMessage().contains("no user record"))
                    alertDialog.noActionDialog("Email is not registered, please sign up first.");
                else if(e.getMessage().contains("network"))
                    alertDialog.noActionDialog("Can't connect to internet. Please check you network settings.");
            }
        };

        mAuth.sendPasswordResetEmail(email).addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);
    }
}
